import axios from '@/libs/api.request'
import rcRequest from 'rc-request'

export const login = ({ userName, password }) => {
  const data = {
    userName,
    password
  }
  return axios.request({
    url: 'login',
    data,
    method: 'post'
  })
}

export const getUserInfo = (token) => {
  return axios.request({
    url: 'get_info',
    params: {
      token
    },
    method: 'get'
  })
}

export const logout = (token) => {
  return axios.request({
    url: 'logout',
    method: 'post'
  })
}

export const getUnreadCount = () => {
  return axios.request({
    url: 'message/count',
    method: 'get'
  })
}

export const getMessage = () => {
  return new Promise(async (resolve, reject) => {
    const params = { method: 'post', url: '/notice/select_notice_list' };
    const { data: { data: readed } } = await rcRequest({ ...params, data: { status: 1 } });
    const { data: { data: unread } } = await rcRequest({ ...params, data: { status: 0 } });

    readed.map(item => item.msg_id = item.id * 1)
    unread.map(item => item.msg_id = item.id * 1)

    resolve({ data: { unread, readed, trash: [] } });
  })
}

export const getContentByMsgId = msg_id => {
  return axios.request({
    url: 'message/content',
    method: 'get',
    params: {
      msg_id
    }
  })
}

export const hasRead = msg_id => {
  return rcRequest({
    method: 'post',
    url: '/notice/update_notice_status',
    data: { notice_id: msg_id, status: 1 },
  });
}

export const removeReaded = msg_id => {
  return rcRequest({
    method: 'post',
    url: '/notice/delete_notice_info',
    data: { notice_id: msg_id },
  })
}

export const restoreTrash = msg_id => {
  return axios.request({
    url: 'message/restore',
    method: 'post',
    data: {
      msg_id
    }
  })
}
