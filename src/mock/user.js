import Mock from 'mockjs'
import { unread, readed, trash } from '@/mock/data/message-data'

const Random = Mock.Random

export const getMessageInit = () => {
  return { unread, readed, trash };
}

export const getContentByMsgId = ({ url }) => {
  const flag = 'msg_id=';
  const index = url.indexOf(flag);
  const msg_id = url.substr(index+flag.length, url.length);
  return [...unread, ...readed, ...trash].filter(item => item.msg_id == msg_id)[0]['content'];
}

export const hasRead = () => {
  return true
}

export const removeReaded = () => {
  return true
}

export const restoreTrash = () => {
  return true
}

export const messageCount = () => {
  return 3
}
